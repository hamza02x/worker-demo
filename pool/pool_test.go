package pool_test

import (
	"encoding/json"
	"sync"
	"testing"
	"time"

	"gitlab.com/hamza02x/worker-demo/pool"
)

const url = "http://httpbin.org/get"

type httpBinResponse struct {
	URL string `json:"url"`
}

func TestGet(t *testing.T) {

	var wg sync.WaitGroup

	for i := 0; i < 400; i++ {

		wg.Add(1)

		go func() {

			time.Sleep(100 * time.Millisecond)

			var data httpBinResponse

			if err := json.Unmarshal(pool.Get(url), &data); err == nil {

				if data.URL != url {
					t.Errorf("Invalid response data: %v", data)
				}

			} else {
				t.Errorf("Error unmarshal: %v", err)
			}

			wg.Done()
		}()
	}

	wg.Wait()
}
