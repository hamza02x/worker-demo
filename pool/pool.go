package pool

import (
	"fmt"
	"io/ioutil"
	"net/http"
)

func createUrlWorker(id int, urlJobs <-chan string, urlResults chan<- []byte) {

	for url := range urlJobs {

		resp, err := http.Get(url)

		if err != nil {
			fmt.Println("Url Worker", id, "failed, where url is", url)
			urlResults <- nil
			continue
		}

		bytes, err := ioutil.ReadAll(resp.Body)

		if err != nil {
			fmt.Println("Error reading response body", err)
		}

		resp.Body.Close()

		urlResults <- bytes
	}
}

var (
	jobs    = make(chan string, 100)
	results = make(chan []byte, 100)
)

func init() {
	for i := 0; i < 100; i++ {
		go createUrlWorker(i, jobs, results)
	}
}

func Get(url string) []byte {
	jobs <- url
	return <-results
}
